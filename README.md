# RamanSpectroscopyFit
Fits Raman Spectroscopy data to a double Lorentzian peak equation using genetic algorithm for initial parameter estimation.

This example requires scipy and matplotlib. To run the example, type:

python3 RamanSpectroscopyFit.py

at a command prompt. Please let me know if you have any questions, I will be glad to help.
